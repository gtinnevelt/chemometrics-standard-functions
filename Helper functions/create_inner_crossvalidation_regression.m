function [trainset_inner, testset_inner] = create_inner_crossvalidation_regression(trainset, sample_ID, Labels)
%%

%%
if isempty(Labels)
    Labels = ones(length(sample_ID),1);
end
trainset_inner = cell(size(trainset,2),1);
testset_inner = cell(size(trainset,2),1);
for l1 = 1:size(trainset,2)
    [trainset_inner{l1}, testset_inner{l1}] = create_trainset_testset_double_cv_regression(sample_ID(trainset(:,l1)),[], Labels(trainset(:,l1)));
end

%%

