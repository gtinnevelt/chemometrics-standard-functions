function [trainset, testset] = create_trainset_testset_double_cv_regression(sample_ID,cv, Labels)
%%

%%
if isempty(Labels)
    Labels = ones(length(sample_ID),1);
end
[~, ia, ic] = unique([sample_ID Labels], 'stable', 'rows');

if isempty(cv)
    
    if length(ia) > 50 % use 5 crossvalidation if the classes are well distributed
        cv(1) = 5; %maybe add something to make it easier diviable by all samples.
        cv(2) = 10;
        x_message = ['Function performs ' num2str(cv(1)) ' fold cross validation with 10 iterations.'];
        disp(x_message)
    else %if the number of subjects is lower than 50, always use leave one out cv
        x_message = 'Function performs leave one out.';
        disp(x_message)
        cv(1) = length(ia);
        cv(2) = 1;
    end
else
    x_message = ['Function performs ' num2str(cv(1)) ' fold cross validation with ' num2str(cv(2)) ' iterations.'];
    disp(x_message)
end

%% Divide data according to crossvalidation

nm_folds = cv(1); % number of folds

m = ones(nm_folds,1);
for l1 = 1:nm_folds
    m(l1) = round(length(ia)/nm_folds*l1); %divide the case
end
m1 = [1; m(1:end-1)+1];

% perform crossvalidation
trainset = zeros(length(ia),cv(2)*nm_folds);
for l2 = 1:cv(2) % number of iterations
    R = ia(randperm(length(ia)));
    for l1 = 1:nm_folds % number of folds
        R_test = [R(m1(l1):m(l1))];
        R_train = setdiff(R, R_test);
        trainset(R_train,(l2-1)*nm_folds+l1) = 1;
    end
end

trainset = trainset(ic,:);
trainset = logical(trainset);
testset = ~trainset;

end