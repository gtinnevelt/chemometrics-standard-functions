function [W,T,P,Q, R] = plsfunction(X,Y,ncomp)
%%
% Function performs PLS regression or discrimant analysis depending on the
% input of Y
%
% Input:
%       X = datamatrix with size m samples by n variables
%       Y = labelmatrix with size m samples by n labels
%               in case of discriminant analysis Y is binary
%       ncomp = the maximum number of components
%
% Output:
%      W = weights with size m samples by ncomp components
%      T = Scores of X with size m samples by ncomp components
%      P = Loadings of X with size m samples by ncomp components
%      Q = Loadings of Y with size m samples by ncomp components
%
%
% Written by G.H. Tinnevelt and O.V. Lushchikova at Radboud University at
% 10 February 2016
% improved the speed and removed some unnecary lines 28 april 2023 GH Tinnevelt
%%

[m, n] = size(X); % size of X

%pre initialize variables
W = zeros(n,ncomp);
T = zeros(m,ncomp);
P = zeros(n,ncomp);
Q = zeros(size(Y,2),ncomp);

for l1 = 1:ncomp
    [U,~,~] = svd(X'*Y,'econ'); 
    W(:,l1) = U(:,1); %PLS X weights
    T(:,l1) = X*W(:,l1); % PLS X scores
    P(:,l1) = X'*T(:,l1)/(T(:,l1)'*T(:,l1)); %PLS X loadings
    Q(:,l1) = Y'*T(:,l1)/(T(:,l1)'*T(:,l1)); %PLS Y loadings
    
    X = X - T(:,l1)*P(:,l1)';%deflate X
    Y = Y - T(:,l1)*Q(:,l1)';%deflate Y
end
R = W/(P'*W);