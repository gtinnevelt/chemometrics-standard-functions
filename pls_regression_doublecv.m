function [yhat_total, RMSECV, R_CV, RMSEP, R_P, ncomp_selected] = pls_regression_doublecv(X, Y, trainset, testset, trainset_inner, testset_inner, max_comp)
%%

% Y should be samples x variables
%%
n_iterations = size(trainset,2); % number of iterations
n_repeats = sum(testset(1,:),2);
n_folds = n_iterations/n_repeats;
ncomp_selected = zeros(1,size(testset,2));
yhat_total = zeros(size(Y,1), n_repeats);

MSECV = 0;
for l1 = 1:n_iterations
    X_train = X(trainset(:,l1),:);
    X_mean = mean(X_train);
    X_train = X_train - X_mean;
    X_test = X(testset(:,l1),:) - X_mean;
    Y_train = Y(trainset(:,l1),:);
    Y_mean = mean(Y_train);
    Y_train = Y_train - Y_mean;
    MSE = ones(1, max_comp);
    yhat = ones(length(Y_train), max_comp);
    for l2 = 1:size(testset_inner{l1},2)
        [~,~,~,Q, R] = plsfunction(X_train(trainset_inner{l1}(:,l2),:), Y_train(trainset_inner{l1}(:,l2)),max_comp);
        for l3 = 1:max_comp
            yhat(testset_inner{l1}(:,l2),l3) = X_train(testset_inner{l1}(:,l2),:)*R(:,1:l3)*Q(1:l3)';
        end
    end
    for l3 = 1:max_comp
        MSE(:,l3) = mean((Y_train - yhat(:,l3)).^2);
    end
    [~, ncomp_selected(l1)] = min(MSE);
    MSECV = MSECV + MSE(ncomp_selected(l1));

    [~,~,~,Q, R] = plsfunction(X_train, Y_train,ncomp_selected(l1));
    yhat_total(testset(:,l1),ceil(l1/n_folds)) = X_test*R(:,1:ncomp_selected(l1))*Q(:,1:ncomp_selected(l1))' + Y_mean; 
end
MSEP = mean((Y - yhat_total).^2);
RMSEP = sqrt(MSEP);
MSECV = MSECV/n_iterations;
RMSECV = sqrt(MSECV);
MSY = mean((Y-mean(Y)).^2);
R_CV = 1 - MSECV/MSY;
R_P = 1 - MSEP/MSY;
